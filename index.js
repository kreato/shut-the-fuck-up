import { Plugin } from '@vizality/entities';
const Settings = require('./components/SettingsClass.jsx')
import { getModule } from '@vizality/webpack';
import { unpatch, patch } from '@vizality/patcher';
export default class ExamplePluginSettings extends Plugin {
    start () {
    // *waves*
    const MessageContent = getModule(m => m.type && m.type.displayName === 'MessageContent')
    patch('sud-message', MessageContent, 'type', ([{ message }], res) => {
      if (message.author.id === this.settings.get('example-text-input'))
        res.props.children[0] = this.settings.get('example-text-input2')
      return res
    })
    MessageContent.type.displayName = 'MessageContent'
  }

  stop () {
    // *waves*
     unpatch('sud-message')
  }
}
