import React, { Component } from 'react';

import { TextInput, Checkbox } from '@vizality/components/settings';
import { getModule, getModuleByDisplayName } from '@vizality/webpack';

export default class Settings extends Component {
  constructor (props) {
    super(props);
    this.Flex = getModuleByDisplayName('Flex');
    this.classes = {
      flexClassName: `${this.Flex.Direction.HORIZONTAL} ${this.Flex.Justify.START} ${this.Flex.Align.STRETCH} ${this.Flex.Wrap.NO_WRAP}`,
      alignCenter: getModule('alignCenter')
    };
  }

  render () {
    const { getSetting, updateSetting, toggleSetting } = this.props;
    const { classes } = this;

    return (
      <>
        
        <div className={classes.flexClassName}>
          <div className={getModule('flexChild').flexChild} style={{ flex: '1 1 50%' }}>
          </div>
          <div className={getModule('flexChild').flexChild} style={{ flex: '1 1 50%' }}>
          </div>
        </div>
       <TextInput
            note='Put the discord id of a user that needs to shut the fuck up.'
            defaultValue={getSetting('example-text-input')}
            required={false}
            disabled={!getSetting('example-disabled-switch-item', true)}
            onChange={val => updateSetting('example-text-input', val)}
          >
           Discord ID
          </TextInput>
                 <TextInput
            note='Put the shut up text'
            defaultValue={getSetting('example-text-input2', 'SHUT THE FUCK UP')}
            required={false}
            disabled={!getSetting('example-disabled-switch-item2', true)}
            onChange={val => updateSetting('example-text-input2', val)}
          >
            Shut up text
          </TextInput>
        {/* <RegionSelector
          disabled={false}
          error={false}
          onClick={ret => {
            // console.log(ret);
            getModule('RegionSelectModal')({
              onChange (...args) {
                // console.log(args);
              }
            });
          }}
          region={{
            custom: false,
            deprecated: false,
            id: 'us-south',
            name: 'US South',
            optimal: true,
            vip: false
          }}
        >
          Just a simple region selector.
        </RegionSelector> */}
        {/* <PermissionOverrideItem
          disabled={false}
          hideBorder={false} // divider
          note={'Members with this permission can change the channel\'s name or delete it.'}
          onChange={''}
          value={'ALLOW'} // 'DENY', 'ALLOW', 'PASSTHROUGH'
        >
          Just a simple permission override item.
        </PermissionOverrideItem> */}
      </>
    );
  }
}
